import React from 'react'
import { Button, Nav, Navbar } from 'react-bootstrap'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { setAlert } from '../store/actions/alertActions'
import { LOGOUT } from '../store/actions/authActions'
import { store } from '../store/store'

const Navigation = () => {
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated)
  const guestLinks = (
    <>
      <Navbar bg='dark' variant='dark' className='justify-content-between'>
        <Navbar.Brand>Admin Panel</Navbar.Brand>
        {/* <Nav>
          <Nav>
            <Link className='text-light mr-3' to='/'>
              Login
            </Link>
          </Nav>
          <Nav>
            <Link className='text-light' to='/signup'>
              Signin
            </Link>
          </Nav>
        </Nav> */}
      </Navbar>
    </>
  )
  const authLinks = (
    <>
      <Navbar bg='dark' variant='dark' className='justify-content-between'>
        <Navbar.Brand>Admin Panel</Navbar.Brand>
        <Nav>
          <Nav>
            <Link className='text-light mr-3' to='/articles'>
              Articles
            </Link>
          </Nav>
          <Nav>
            <Link className='text-light mr-3' to='/users'>
              Users
            </Link>
          </Nav>
          <Nav>
            <Link className='text-light mr-3' to='/admins'>
              Admins
            </Link>
          </Nav>
          <Button
            onClick={() => {
              store.dispatch({ type: LOGOUT })
              store.dispatch(setAlert('Logged Out', 'success'))
            }}
            variant='outline-light'
            size='sm'>
            Log Out
          </Button>
        </Nav>
      </Navbar>
    </>
  )
  return <>{isAuthenticated ? authLinks : guestLinks}</>
}

export default Navigation
