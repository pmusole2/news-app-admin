import React, { useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { signupHandler } from '../../store/actions/authActions'

const Signup = ({ showModal, setShowModal }) => {
  const [inputs, setInputs] = useState({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    password2: '',
  })
  const dispatch = useDispatch()
  return (
    <Modal show={showModal} centered onHide={() => setShowModal(false)}>
      <Modal.Header closeButton>
        <Modal.Title>Create Admin Account</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group controlId='formFirstName'>
            <Form.Label>First Name</Form.Label>
            <Form.Control
              placeholder='First Name'
              onChange={e =>
                setInputs({ ...inputs, firstName: e.target.value })
              }
              required
            />
          </Form.Group>
          <Form.Group controlId='formLaststName'>
            <Form.Label>Last Name</Form.Label>
            <Form.Control
              placeholder='Last Name'
              onChange={e => setInputs({ ...inputs, lastName: e.target.value })}
              required
            />
          </Form.Group>
          <Form.Group controlId='formBasicEmail'>
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type='email'
              placeholder='Enter email'
              onChange={e => setInputs({ ...inputs, email: e.target.value })}
              required
            />
          </Form.Group>
          <Form.Group controlId='formBasicPassword'>
            <Form.Label>Password</Form.Label>
            <Form.Control
              type='password'
              placeholder='Password'
              onChange={e => setInputs({ ...inputs, password: e.target.value })}
              required
            />
          </Form.Group>
          <Form.Group controlId='formBasicPassword2'>
            <Form.Label>Confirm Password</Form.Label>
            <Form.Control
              type='password'
              placeholder='Confirm Password'
              onChange={e =>
                setInputs({ ...inputs, password2: e.target.value })
              }
              required
            />
          </Form.Group>
          <Button
            disabled={
              !inputs.email ||
              !inputs.password ||
              !inputs.firstName ||
              !inputs.lastName ||
              !inputs.password2
            }
            block
            type='submit'
            variant='primary'
            onClick={e => {
              e.preventDefault()
              if (inputs.password !== inputs.password2) {
                alert('Passwords do not match!')
                return false
              }
              dispatch(signupHandler(inputs))
              setShowModal(false)
            }}>
            Submit
          </Button>
        </Form>
      </Modal.Body>
    </Modal>
  )
}

export default Signup
