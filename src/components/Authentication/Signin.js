import React, { useState } from 'react'
import { Button, Form } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { signinHandler } from '../../store/actions/authActions'

const Signin = () => {
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated)
  const [inputs, setInputs] = useState({
    email: '',
    password: '',
  })
  const dispatch = useDispatch()

  if (isAuthenticated) {
    return <Redirect to='/articles' />
  }

  return (
    <div
      className='cardStyles container'
      style={{ ...styles, marginTop: '10%', borderRadius: 10 }}>
      <h3 className='text-center'>Sign into your Account</h3>
      <Form>
        <Form.Group controlId='formBasicEmail'>
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type='email'
            placeholder='Enter email'
            onChange={e => setInputs({ ...inputs, email: e.target.value })}
            required
          />
        </Form.Group>
        <Form.Group controlId='formBasicPassword'>
          <Form.Label>Password</Form.Label>
          <Form.Control
            type='password'
            placeholder='Password'
            onChange={e => setInputs({ ...inputs, password: e.target.value })}
            required
          />
        </Form.Group>
        <Button
          disabled={!inputs.email || !inputs.password}
          block
          type='submit'
          variant='primary'
          onClick={e => {
            e.preventDefault()
            dispatch(signinHandler(inputs))
          }}>
          Submit
        </Button>
        <p className='text-center pt-2'>
          Don't have an account? Contact an Admin
        </p>
      </Form>
    </div>
  )
}

const styles = {
  padding: 25,
}

export default Signin
