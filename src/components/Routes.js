import React from 'react'
import { Route, Switch } from 'react-router-dom'
import AdminComponent from './admins/AdminComponent'
import Alert from './Alert'
import ArticleOverview from './articles/ArticleOverview'
import Signin from './Authentication/Signin'
import PrivateRoute from './PrivateRoute'
import UsersOverview from './users/UsersOverview'

const Routes = props => {
  return (
    <section>
      <Alert />
      <Switch>
        <Route exact path='/' component={Signin} />
        <PrivateRoute exact path='/users' component={UsersOverview} />
        <PrivateRoute exact path='/articles' component={ArticleOverview} />
        <PrivateRoute exact path='/admins' component={AdminComponent} />
      </Switch>
    </section>
  )
}

export default Routes
