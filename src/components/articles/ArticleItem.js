import React, { useState } from 'react'
import { Button, Card } from 'react-bootstrap'
import Moment from 'react-moment'
import ArticleModal from './ArticleModal'
import CreateModal from './CreateModal'

const ArticleItem = ({ article }) => {
  const [modalOpen, setModalOpen] = useState(false)
  const [showEditModal, setShowEditModal] = useState(false)

  return (
    <>
      <ArticleModal
        modalOpen={modalOpen}
        setModalOpen={setModalOpen}
        article={article}
        showEditModal={showEditModal}
        setShowEditModal={setShowEditModal}
      />
      <CreateModal
        article={article}
        showModal={showEditModal}
        setShowModal={setShowEditModal}
      />
      <Card className='m-2' style={{ borderRadius: 10 }}>
        <Card.Body>
          <Card.Title>{article.title}</Card.Title>
          <Card.Text>
            Posted By:{' '}
            <span className='font-weight-bold'>{article.admin.name}</span>
          </Card.Text>
          <Card.Text>
            Posted:{' '}
            <span className='font-weight-bold'>
              <Moment format='hh:mm DD/MM/YYYY'>{article.postedAt}</Moment>
            </span>
          </Card.Text>
          <Button
            block
            variant='outline-primary'
            onClick={() => setModalOpen(true)}>
            View
          </Button>
        </Card.Body>
      </Card>
    </>
  )
}

export default ArticleItem
