import React from 'react'
import { Button, Modal } from 'react-bootstrap'
import Moment from 'react-moment'
import { useDispatch } from 'react-redux'
import { deleteArticle } from '../../store/actions/articleActions'

const ArticleModal = ({
  modalOpen,
  setModalOpen,
  article: { admin, articleType, body, category, postedAt, title, _id },
  showEditModal,
  setShowEditModal,
}) => {
  const dispatch = useDispatch()
  return (
    <Modal centered onHide={() => setModalOpen(false)} show={modalOpen}>
      <Modal.Header closeButton>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className='d-flex justify-content-around'>
          <p className='font-weight-bold'>Type: {articleType}</p>
          <p className='font-weight-bold'>Category: {category}</p>
          <p className='font-weight-bold'>By: {admin.name}</p>
        </div>
        <p className='text-center'>
          Date Posted:{' '}
          <span className='font-weight-bold'>
            <Moment format='hh:mm DD/MM/YYYY'>{postedAt}</Moment>
          </span>
        </p>
        <p className='text-center'>{body}</p>
        <Button
          variant='outline-primary'
          onClick={() => {
            setModalOpen(false)
            setShowEditModal(true)
          }}
          block>
          Edit Article
        </Button>
        <Button
          variant='outline-danger'
          block
          onClick={() => dispatch(deleteArticle(_id))}>
          Delete
        </Button>
      </Modal.Body>
    </Modal>
  )
}

export default ArticleModal
