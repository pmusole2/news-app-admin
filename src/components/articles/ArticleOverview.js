import React, { useEffect, useState } from 'react'
import { Button } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { getAllArticles } from '../../store/actions/articleActions'
import Loader from '../Loader'
import ArticleItem from './ArticleItem'
import CreateModal from './CreateModal'

const ArticleOverview = () => {
  const [showModal, setShowModal] = useState(false)
  const { articles, articlesLoading } = useSelector(state => state.articles)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getAllArticles())
    // eslint-disable-next-line
  }, [])

  return (
    <div
      className='cardStyles container'
      style={{
        padding: 25,
        marginTop: '5%',
        borderRadius: 10,
        marginBottom: '5%',
      }}>
      <CreateModal showModal={showModal} setShowModal={setShowModal} />
      <Button
        block
        variant='outline-success'
        onClick={() => setShowModal(true)}>
        New Article
      </Button>
      {articlesLoading ? (
        <Loader />
      ) : articles && !articlesLoading ? (
        articles.map(article => (
          <ArticleItem key={article._id} article={article} />
        ))
      ) : (
        <p className='h4'>There are no articles here!</p>
      )}
    </div>
  )
}

export default ArticleOverview
