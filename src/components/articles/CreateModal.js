import React, { useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import {
  createNewArticle,
  editArticle,
} from '../../store/actions/articleActions'

const CreateModal = ({ showModal, setShowModal, article }) => {
  const [inputs, setInputs] = useState({
    title: article ? article.title : '',
    body: article ? article.body : '',
    category: article ? article.category : '',
    articleType: article ? article.articleType : '',
  })
  const dispatch = useDispatch()
  return (
    <Modal show={showModal} centered onHide={() => setShowModal(false)}>
      <Modal.Header closeButton>
        <Modal.Title>
          {article ? 'Edit Article' : 'Post New Article'}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group controlId='articleTitle'>
            <Form.Label>Title</Form.Label>
            <Form.Control
              value={inputs.title}
              onChange={e => setInputs({ ...inputs, title: e.target.value })}
              type='text'
              placeholder='Enter Article Title'
              required
            />
          </Form.Group>
          <Form.Group controlId='articleBody'>
            <Form.Label>Article Text</Form.Label>
            <Form.Control
              value={inputs.body}
              as='textarea'
              rows='5'
              required
              onChange={e => setInputs({ ...inputs, body: e.target.value })}
            />
          </Form.Group>
          <Form.Group controlId='articleCategory'>
            <Form.Label>Select Category</Form.Label>
            <Form.Control
              required
              value={inputs.category}
              onChange={e => setInputs({ ...inputs, category: e.target.value })}
              as='select'>
              <option value='Technology'>Technology</option>
              <option value='Covid-19'>Covid-19</option>
              <option value='Sports'>Sports</option>
              <option value='Entertainment'>Entertainment</option>
              {/* <option value='Lifestyle'>Lifestyle</option> */}
            </Form.Control>
          </Form.Group>
          <Form.Group controlId='articleType'>
            <Form.Label>Select Viewers</Form.Label>
            <Form.Control
              value={inputs.articleType}
              required
              onChange={e =>
                setInputs({ ...inputs, articleType: e.target.value })
              }
              as='select'>
              <option value='Free'>Free</option>
              <option value='Subscribers'>Subscribers</option>
            </Form.Control>
          </Form.Group>
          <Button
            variant='outline-primary'
            type='submit'
            block
            onClick={e => {
              e.preventDefault()
              if (!article) {
                dispatch(createNewArticle(inputs))
                setShowModal(false)
              } else if (article) {
                dispatch(editArticle(article._id, inputs))
                setShowModal(false)
              }
            }}>
            {article ? 'Edit' : 'Submit'}
          </Button>
        </Form>
      </Modal.Body>
    </Modal>
  )
}

export default CreateModal
