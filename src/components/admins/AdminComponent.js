import React, { useEffect, useState } from 'react'
import { Button } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { getAllAdminAccounts } from '../../store/actions/adminActions'
import Signup from '../Authentication/Signup'
import Loader from '../Loader'
import AdminItem from './AdminItem'

const AdminComponent = () => {
  const { admins, adminLoading } = useSelector(state => state.admin)
  const [showModal, setShowModal] = useState(false)
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(getAllAdminAccounts())
    // eslint-disable-next-line
  }, [])

  return (
    <div
      style={{
        padding: 25,
        marginTop: '5%',
        borderRadius: 10,
        marginBottom: '5%',
      }}
      className='container cardStyles'>
      <Signup showModal={showModal} setShowModal={setShowModal} />
      <Button
        variant='outline-primary'
        block
        onClick={() => setShowModal(true)}>
        {' '}
        Add Admin{' '}
      </Button>
      {adminLoading ? (
        <Loader />
      ) : admins && !adminLoading ? (
        admins.map(admin => <AdminItem key={admin._id} admin={admin} />)
      ) : (
        <p className='text-center'>There are currently no admins</p>
      )}
    </div>
  )
}

export default AdminComponent
