import React from 'react'
import { Button, Modal } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import {
  changeAdminAccountType,
  changeAdminStatus,
  deleteAdminAccount,
} from '../../store/actions/adminActions'

const ModalComponent = ({
  accountStatus,
  accountType,
  email,
  firstName,
  lastName,
  modalOpen,
  setModalOpen,
  _id,
}) => {
  const dispatch = useDispatch()
  const status = {
    status: accountStatus === 'Active' ? 'Disabled' : 'Active',
  }
  const type = {
    type: accountType === 'Admin' ? 'Free' : 'Admin',
  }
  return (
    <>
      <Modal centered show={modalOpen} onHide={() => setModalOpen(false)}>
        <Modal.Header closeButton>
          <Modal.Title>{`${firstName} ${lastName}`}</Modal.Title>
        </Modal.Header>
        <Modal.Body>Email Address: {email}</Modal.Body>
        <Modal.Body>Account Type: {accountType}</Modal.Body>
        <Modal.Body>
          <p>
            {' '}
            Account Status:{' '}
            <span
              className={`${
                accountStatus === 'Active' ? 'text-success' : 'text-danger'
              } `}>
              {accountStatus}
            </span>
          </p>
        </Modal.Body>
        <Modal.Body>
          <div className='d-flex justify-content-around'>
            <Button
              variant='outline-info'
              onClick={() => {
                dispatch(changeAdminAccountType(_id, type))
                setModalOpen(false)
              }}>
              {accountType === 'Admin' ? 'Revoke Access' : 'Enable Access'}
            </Button>
            <Button
              variant='outline-secondary'
              onClick={() => {
                dispatch(changeAdminStatus(_id, status))
                setModalOpen(false)
              }}>
              {accountStatus === 'Active' ? 'Disable' : 'Enable'}
            </Button>
            <Button
              variant='outline-danger'
              onClick={() => {
                dispatch(deleteAdminAccount(_id))
                setModalOpen(false)
              }}>
              Delete
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    </>
  )
}

export default ModalComponent
