import React from 'react'
import { Button, Modal } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import {
  changeUserSubscription,
  deleteUserAccount,
} from '../../store/actions/adminActions'

const UserModal = ({
  accountStatus,
  accountType,
  email,
  firstName,
  lastName,
  modalOpen,
  setModalOpen,
  _id,
}) => {
  const dispatch = useDispatch()
  const status = {
    status: accountStatus === 'Free' ? 'Subscribed' : 'Free',
  }
  return (
    <>
      <Modal centered show={modalOpen} onHide={() => setModalOpen(false)}>
        <Modal.Header closeButton>
          <Modal.Title>{`${firstName} ${lastName}`}</Modal.Title>
        </Modal.Header>
        <Modal.Body>Email Address: {email}</Modal.Body>
        <Modal.Body>Account Type: {accountType}</Modal.Body>
        <Modal.Body>
          <p>
            {' '}
            Account Status:{' '}
            <span
              className={`${
                accountStatus === 'Subscribed' ? 'text-success' : 'text-danger'
              } `}>
              {accountStatus}
            </span>
          </p>
        </Modal.Body>
        <Modal.Body>
          <div className='d-flex justify-content-around'>
            <Button
              variant='outline-secondary'
              onClick={() => dispatch(changeUserSubscription(_id, status))}>
              {accountStatus === 'Free'
                ? 'Make Subscriber'
                : 'Revoke Subscription'}
            </Button>
            <Button
              variant='outline-danger'
              onClick={() => dispatch(deleteUserAccount(_id))}>
              Delete
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    </>
  )
}

export default UserModal
