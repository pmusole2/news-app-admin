import React, { useState } from 'react'
import { Button } from 'react-bootstrap'
import UserModal from './UserModal'

const UserItem = ({
  user: { accountType, accountStatus, firstName, lastName, email, _id },
}) => {
  const [modalOpen, setModalOpen] = useState(false)
  return (
    <>
      <UserModal
        modalOpen={modalOpen}
        setModalOpen={setModalOpen}
        accountType={accountType}
        accountStatus={accountStatus}
        firstName={firstName}
        lastName={lastName}
        email={email}
        _id={_id}
      />
      <div
        style={{ padding: 15, marginTop: '5%' }}
        className='container cardStyles'>
        <p className='text-center h4'>{`${firstName} ${lastName}`}</p>
        <p className='text-center'>
          {' '}
          Account Status:{' '}
          <span
            className={`${
              accountStatus === 'Subscribed' ? 'text-success' : 'text-danger'
            } `}>
            {accountStatus}
          </span>{' '}
        </p>
        <Button
          block
          onClick={() => setModalOpen(!modalOpen)}
          variant='outline-primary'>
          Options
        </Button>
      </div>
    </>
  )
}

export default UserItem
