import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getAllUserAccounts } from '../../store/actions/adminActions'
import Loader from '../Loader'
import UserItem from './UsersItem'

const UsersOverview = () => {
  const { users, usersLoading } = useSelector(state => state.users)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getAllUserAccounts())
    // eslint-disable-next-line
  }, [])

  return (
    <div
      style={{
        padding: 25,
        marginTop: '5%',
        borderRadius: 10,
        marginBottom: '5%',
      }}
      className='container cardStyles'>
      {usersLoading ? (
        <Loader />
      ) : users && !usersLoading ? (
        users.map(user => <UserItem key={user._id} user={user} />)
      ) : (
        <p className='text-center'>There are currently no users</p>
      )}
    </div>
  )
}

export default UsersOverview
