import React from 'react'
import { useSelector } from 'react-redux'
import { Redirect, Route } from 'react-router-dom'
import Loader from './Loader'

const PrivateRoute = ({ component: Component, ...rest }) => {
  const { isAuthenticated, authLoading } = useSelector(state => state.auth)
  return (
    <Route
      {...rest}
      render={props =>
        authLoading ? (
          <Loader />
        ) : isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect to='/' />
        )
      }
    />
  )
}

export default PrivateRoute
