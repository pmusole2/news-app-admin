import React from 'react'
import loader from '../assets/Loader.gif'

const Loader = () => {
  return (
    <div className='d-flex justify-content-center align-items-center'>
      <img src={loader} alt='Loading' />
    </div>
  )
}

export default Loader
