import Axios from 'axios'
import { LOGOUT } from './actions/authActions'
import { store } from './store'

export const requestUri = Axios.create({
  baseURL: 'https://news-app-server.herokuapp.com',
  headers: {
    'Content-Type': 'application/json',
  },
})

requestUri.interceptors.response.use(
  res => res,
  err => {
    if (err.response.status === 401) {
      store.dispatch({ type: LOGOUT })
    }
    return Promise.reject(err)
  }
)

// Save Auth Token
export const setAuthToken = token => {
  if (token) {
    requestUri.defaults.headers.common['x-auth-token'] = token
    localStorage.setItem('token', token)
  } else {
    delete requestUri.defaults.headers.common['x-auth-token']
    localStorage.removeItem('token')
  }
}
