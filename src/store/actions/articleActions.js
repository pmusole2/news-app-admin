// IMPORTS
import { requestUri } from '../helpers'
import { setAlert } from './alertActions'

// ACTION TYPES
export const GET_ALL_ARTICLES = 'GET_ALL_ARTICLES'
export const GET_ALL_ARTICLES_FAILED = 'GET_ALL_ARTICLES_FAILED'
export const DELETE_ARTICLE = 'DELETE_ARTICLE'
export const DELETE_ARTICLE_FAILED = 'DELETE_ARTICLE_FAILED'
export const CREATE_ARTICLE = 'CREATE_ARTICLE'
export const CREATE_ARTICLE_FAILED = 'CREATE_ARTICLE_FAILED'
export const EDIT_ARTICLE = 'EDIT_ARTICLE'
export const EDIT_ARTICLE_FAILED = 'EDIT_ARTICLE_FAILED'
export const SET_ARTICLES_LOADING = 'SET_ARTICLES_LOADING'

// SET ARTICLES LOADING
export const setArticlesLoading = () => async dispatch => {
  dispatch({
    type: SET_ARTICLES_LOADING,
  })
}

// GET ALL ARTCLES
export const getAllArticles = () => async dispatch => {
  dispatch(setArticlesLoading())
  try {
    const res = await requestUri.get(`/api/articles`)

    dispatch({
      type: GET_ALL_ARTICLES,
      payload: res.data,
    })
  } catch (error) {
    dispatch({
      type: GET_ALL_ARTICLES_FAILED,
      payload: error.response.data,
    })
    dispatch(
      setAlert(
        'Error Occured, Check your internet and reload the page',
        'danger'
      )
    )
  }
}

// CREATE NEW ARTICLE
export const createNewArticle = inputs => async dispatch => {
  dispatch(setArticlesLoading())
  const body = JSON.stringify(inputs)
  try {
    const res = await requestUri.post(`/api/articles/new`, body)
    dispatch({
      type: CREATE_ARTICLE,
      payload: res.data,
    })
    dispatch(setAlert('Article Created Successfully', 'success'))
  } catch (error) {
    dispatch({
      type: CREATE_ARTICLE_FAILED,
      payload: error.response.data,
    })
    dispatch(setAlert(error.response.data.message, 'danger'))
  }
}

// DELETE ARTICLE
export const deleteArticle = id => async dispatch => {
  dispatch(setArticlesLoading())
  try {
    const res = await requestUri.delete(`/api/articles/${id}`)
    dispatch({
      type: DELETE_ARTICLE,
      payload: { message: res.data, id: id },
    })
    dispatch(setAlert('Article Deleted', 'success'))
  } catch (error) {
    dispatch({
      type: DELETE_ARTICLE_FAILED,
      payload: error.response.data,
    })
    dispatch(setAlert(error.response.data.message, 'danger'))
  }
}

// EDIT ARTICLE
export const editArticle = (id, inputs) => async dispatch => {
  dispatch(setArticlesLoading())
  const body = JSON.stringify(inputs)
  try {
    const res = await requestUri.put(`/api/articles/${id}`, body)
    dispatch({
      type: EDIT_ARTICLE,
      payload: res.data,
    })
    dispatch(setAlert('Article edited', 'success'))
  } catch (error) {
    dispatch({
      type: EDIT_ARTICLE_FAILED,
      payload: error.response.data,
    })
    dispatch(setAlert(error.response.data.message, 'danger'))
  }
}
