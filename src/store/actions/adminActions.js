import { requestUri } from '../helpers'
import { setAlert } from './alertActions'

export const GET_ALL_ADMIN_ACCOUNTS = 'GET_ALL_ADMIN_ACCOUNTS'
export const GET_ALL_ADMIN_ACCOUNTS_FAILED = 'GET_ALL_ADMIN_ACCOUNTS_FAILED'
export const ADMIN_ACC_DELETED = 'ADMIN_ACC_DELETED'
export const ADMIN_ACC_DELETE_FAILED = 'ADMIN_ACC_DELETE_FAILED'
export const ADMIN_ACC_DISABLE = 'ADMIN_ACC_DISABLE'
export const ADMIN_ACC_DISABLE_FAILED = 'ADMIN_ACC_DISABLE_FAILED'
export const REVOKE_ADMIN_ACCESS = 'REVOKE_ADMIN_ACCESS'
export const REVOKE_ADMIN_ACCESS_FAILED = 'REVOKE_ADMIN_ACCESS_FAILED'
export const DELETE_USER_ACC = 'DELETE_USER_ACC'
export const DELETE_USER_ACC_FAILED = 'DELETE_USER_ACC_FAILED'
export const CHANGE_USER_SUB = 'CHANGE_USER_SUB'
export const CHANGE_USER_SUB_FAILED = 'CHANGE_USER_SUB_FAILED'
export const GET_ALL_USER_ACC = 'GET_ALL_USER_ACC'
export const GET_ALL_USER_ACC_FAILED = 'GET_ALL_USER_ACC_FAILED'
export const SET_USER_LOADING = 'SET_USER_LOADING'
export const SET_ADMIN_LOADING = 'SET_ADMIN_LOADING'

// SET USERS LOADING
export const setUsersLoading = () => async dispatch => {
  dispatch({
    type: SET_USER_LOADING,
  })
}

// SET ADMIN LOADING
export const setAdminLoading = () => async dispatch => {
  dispatch({
    type: SET_ADMIN_LOADING,
  })
}

// ADMIN ACCOUNT CHANGES

// Get all Admin Accounts
export const getAllAdminAccounts = () => async dispatch => {
  dispatch(setAdminLoading())
  try {
    const res = await requestUri.get(`/api/admin/all`)
    dispatch({
      type: GET_ALL_ADMIN_ACCOUNTS,
      payload: res.data,
    })
  } catch (error) {
    dispatch({
      type: GET_ALL_ADMIN_ACCOUNTS_FAILED,
      payload: error.response.data,
    })
    dispatch(
      setAlert(
        'Error Occured, Check your internet and reload the page',
        'danger'
      )
    )
  }
}

// Delete Admin Account
export const deleteAdminAccount = id => async dispatch => {
  dispatch(setAdminLoading())
  try {
    const res = await requestUri.delete(`/api/admin/remove/${id}`)

    dispatch({
      type: ADMIN_ACC_DELETED,
      payload: { message: res.data, id: id },
    })
    dispatch(setAlert('Admin Account Deleted Successfully', 'success'))
  } catch (error) {
    dispatch({
      type: ADMIN_ACC_DELETE_FAILED,
      payload: error.response.data,
    })
    dispatch(setAlert(error.response.data.message, 'danger'))
  }
}

// Change Admin Account Type
export const changeAdminAccountType = (id, type) => async dispatch => {
  dispatch(setAdminLoading())
  const body = JSON.stringify(type)
  try {
    console.log(body)
    const res = await requestUri.put(`/api/admin/type/${id}`, body)

    dispatch({
      type: REVOKE_ADMIN_ACCESS,
      payload: res.data,
    })
    dispatch(getAllAdminAccounts())
    dispatch(setAlert('Admin Account Changed Successfully', 'success'))
  } catch (error) {
    dispatch({
      type: REVOKE_ADMIN_ACCESS_FAILED,
      payload: error.response.data,
    })
    dispatch(setAlert(error.response.data.message, 'danger'))
  }
}

// Change Admin Status
export const changeAdminStatus = (id, status) => async dispatch => {
  dispatch(setAdminLoading())
  const body = JSON.stringify(status)
  try {
    const res = await requestUri.put(`/api/admin/status/${id}`, body)

    dispatch({
      type: ADMIN_ACC_DISABLE,
      payload: res.data,
    })
    dispatch(getAllAdminAccounts())
    dispatch(setAlert('Admin Status Changed Successfully', 'success'))
  } catch (error) {
    dispatch({
      type: ADMIN_ACC_DISABLE_FAILED,
      payload: error.response.data,
    })
    dispatch(setAlert(error.response.data.message, 'danger'))
  }
}

// USER ACCOUNT ACTIONS

// GET ALL USER ACCOUNTS
export const getAllUserAccounts = () => async dispatch => {
  dispatch(setUsersLoading())
  try {
    const res = await requestUri.get(`/api/admin/users`)
    dispatch({
      type: GET_ALL_USER_ACC,
      payload: res.data,
    })
  } catch (error) {
    dispatch({
      type: GET_ALL_USER_ACC_FAILED,
    })
    dispatch(
      setAlert(
        'Error Occured, Check your internet and reload the page',
        'danger'
      )
    )
  }
}

// DELETE USER ACCOUNT
export const deleteUserAccount = id => async dispatch => {
  dispatch(setUsersLoading())
  try {
    const res = await requestUri.delete(`/api/admin/users/${id}`)

    dispatch({
      type: DELETE_USER_ACC,
      payload: { message: res.data, id: id },
    })
    dispatch(setAlert('User Account Deleted Successfully', 'success'))
  } catch (error) {
    dispatch({
      type: DELETE_USER_ACC_FAILED,
    })
    dispatch(setAlert(error.response.data.message, 'danger'))
  }
}

// CHANGE USER SUBSCRIPTION
export const changeUserSubscription = (id, status) => async dispatch => {
  dispatch(setUsersLoading())
  const body = JSON.stringify(status)
  try {
    const res = await requestUri.put(`/api/admin/users/${id}`, body)

    dispatch({
      type: CHANGE_USER_SUB,
      payload: res.data,
    })
    dispatch(getAllUserAccounts())
    dispatch(setAlert('User Subscription Changed Successfully', 'success'))
  } catch (error) {
    dispatch({
      type: CHANGE_USER_SUB_FAILED,
      payload: error.response.data,
    })
    dispatch(setAlert(error.response.data.message, 'danger'))
  }
}
