import { requestUri } from '../helpers'
import { getAllAdminAccounts } from './adminActions'
import { setAlert } from './alertActions'

export const SIGNIN_SUCCESS = 'SIGNIN_SUCCESS'
export const SIGNIN_FAILED = 'SIGNIN_FAILED'
export const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS'
export const SIGNUP_FAILED = 'SIGNUP_FAILED'
export const GET_LOGGED_IN_ADMIN = 'GET_LOGGED_IN_ADMIN'
export const GET_LOGGED_IN_ADMIN_FAILED = 'GET_LOGGED_IN_ADMIN_FAILED'
export const SET_AUTH_LOADING = 'SET_AUTH_LOADING'
export const LOGOUT = 'LOGOUT'

// SET AUTH LOADING
export const setAuthLoading = () => async dispatch => {
  dispatch({
    type: SET_AUTH_LOADING,
  })
}

// Signin Handler
export const signinHandler = inputs => async dispatch => {
  dispatch(setAuthLoading())
  const body = JSON.stringify(inputs)
  try {
    const res = await requestUri.post(`/api/admin/auth`, body)
    dispatch({
      type: SIGNIN_SUCCESS,
      payload: res.data,
    })
    dispatch(getCurrentAdmin())
    dispatch(setAlert('Signed in Successfully', 'success'))
  } catch (error) {
    dispatch({
      type: SIGNIN_FAILED,
      // payload: error.response.data,
    })
    dispatch(setAlert(error.response.data.message, 'danger'))
  }
}

// Signup Handler
export const signupHandler = inputs => async dispatch => {
  dispatch(setAuthLoading())
  const body = JSON.stringify(inputs)
  try {
    const res = await requestUri.post(`/api/admin/new`, body)
    dispatch({
      type: SIGNUP_SUCCESS,
      payload: res.data,
    })
    dispatch(setAlert('Admin Added', 'success'))
    dispatch(getAllAdminAccounts())
  } catch (error) {
    dispatch({
      type: SIGNUP_FAILED,
      payload: error.response.data,
    })
    dispatch(setAlert(error.response.data.message, 'danger'))
  }
}

// GET LOGGED IN ADMIN DETAILS
export const getCurrentAdmin = () => async dispatch => {
  dispatch(setAuthLoading())
  try {
    const res = await requestUri.get(`/api/admin`)
    dispatch({
      type: GET_LOGGED_IN_ADMIN,
      payload: res.data,
    })
  } catch (error) {
    dispatch({
      type: GET_LOGGED_IN_ADMIN_FAILED,
      // payload: error.response.data,
    })
    dispatch(
      setAlert("You've been logged out. Please Log into your Account", 'danger')
    )
  }
}
