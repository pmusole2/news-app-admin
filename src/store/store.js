import { createStore, applyMiddleware, combineReducers } from 'redux'
import ReduxThunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import auth from './reducers/authReducer'
import users from './reducers/usersReducer'
import articles from './reducers/articlesReducer'
import admin from './reducers/adminReducers'
import { setAuthToken } from './helpers'
import alert from './reducers/alertReducers'

const initialState = {}

const middleware = [ReduxThunk]

const rootReducer = combineReducers({
  alert,
  auth,
  users,
  articles,
  admin,
})

export const store = createStore(
  rootReducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
)

// Set up a store subscription Listener
let currentState = store.getState()

store.subscribe(() => {
  // Keep Track of previous state
  let previousState = currentState
  currentState = store.getState()
  // If token changes set the value to Headers and LocalStorage
  if (previousState.auth.token !== currentState.auth.token) {
    const token = currentState.auth.token
    setAuthToken(token)
  }
})
