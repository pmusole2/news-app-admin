import {
  GET_LOGGED_IN_ADMIN,
  GET_LOGGED_IN_ADMIN_FAILED,
  LOGOUT,
  SIGNIN_FAILED,
  SIGNIN_SUCCESS,
  SIGNUP_FAILED,
  SIGNUP_SUCCESS,
} from '../actions/authActions'

const initialState = {
  isAuthenticated: false,
  token: localStorage.getItem('token'),
  admin: null,
  authLoading: true,
}

export default (state = initialState, action) => {
  const { type, payload } = action
  switch (type) {
    case SIGNUP_SUCCESS:
    case SIGNIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        token: payload.token,
        authLoading: false,
        errors: null,
      }
    case SIGNIN_FAILED:
    case SIGNUP_FAILED:
    case LOGOUT:
      return {
        ...state,
        token: null,
        isAuthenticated: false,
        admin: null,
        authLoading: false,
        errors: payload,
      }
    case GET_LOGGED_IN_ADMIN:
      return {
        ...state,
        admin: payload,
        isAuthenticated: true,
        authLoading: false,
        errors: null,
      }
    case GET_LOGGED_IN_ADMIN_FAILED:
      return {
        ...state,
        authLoading: false,
        errors: payload,
      }
    default:
      return state
  }
}
