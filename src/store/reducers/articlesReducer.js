import {
  CREATE_ARTICLE,
  CREATE_ARTICLE_FAILED,
  DELETE_ARTICLE,
  DELETE_ARTICLE_FAILED,
  EDIT_ARTICLE,
  EDIT_ARTICLE_FAILED,
  GET_ALL_ARTICLES,
  GET_ALL_ARTICLES_FAILED,
  SET_ARTICLES_LOADING,
} from '../actions/articleActions'

const initialState = {
  articlesLoading: false,
  articles: null,
  errors: null,
  article: null,
}

export default (state = initialState, action) => {
  const { type, payload } = action
  switch (type) {
    case SET_ARTICLES_LOADING:
      return {
        ...state,
        articlesLoading: true,
      }
    case GET_ALL_ARTICLES:
      return {
        ...state,
        articles: payload,
        errors: null,
        articlesLoading: false,
      }
    case GET_ALL_ARTICLES_FAILED:
      return {
        ...state,
        articles: null,
        articlesLoading: false,
        errors: payload,
      }
    case CREATE_ARTICLE:
      return {
        ...state,
        articles: [payload, ...state.articles],
        articlesLoading: false,
        errors: null,
      }
    case CREATE_ARTICLE_FAILED:
      return {
        ...state,
        articlesLoading: false,
        errors: payload,
      }
    case DELETE_ARTICLE:
      return {
        ...state,
        articles: state.articles.filter(article => article._id !== payload.id),
        articlesLoading: false,
        errors: null,
      }
    case DELETE_ARTICLE_FAILED:
      return {
        ...state,
        errors: payload,
        articlesLoading: false,
      }
    case EDIT_ARTICLE:
      // let oldArticle = state.articles.find(article => article._id === payload._id)
      state.articles.filter(article => article._id !== payload._id)
      return {
        ...state,
        articles: [payload, ...state.articles],
        articlesLoading: false,
        errors: null,
      }
    case EDIT_ARTICLE_FAILED:
      return {
        ...state,
        errors: payload,
        articlesLoading: false,
      }
    default:
      return state
  }
}
