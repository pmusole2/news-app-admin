import {
  ADMIN_ACC_DELETED,
  ADMIN_ACC_DELETE_FAILED,
  ADMIN_ACC_DISABLE,
  ADMIN_ACC_DISABLE_FAILED,
  GET_ALL_ADMIN_ACCOUNTS,
  GET_ALL_ADMIN_ACCOUNTS_FAILED,
  REVOKE_ADMIN_ACCESS,
  REVOKE_ADMIN_ACCESS_FAILED,
} from '../actions/adminActions'

const initialState = {
  adminLoading: true,
  admins: false,
  errors: null,
}

export default (state = initialState, action) => {
  const { type, payload } = action
  switch (type) {
    case GET_ALL_ADMIN_ACCOUNTS:
      return {
        ...state,
        admins: payload,
        adminLoading: false,
        errors: null,
      }
    case GET_ALL_ADMIN_ACCOUNTS_FAILED:
      return {
        ...state,
        admins: null,
        adminLoading: false,
        errors: payload,
      }
    case ADMIN_ACC_DELETED:
      return {
        ...state,
        admins: state.admins.filter(admin => admin._id !== payload.id),
        adminLoading: false,
        errors: null,
      }
    case ADMIN_ACC_DELETE_FAILED:
      return {
        ...state,
        errors: payload,
        adminLoading: false,
      }
    case REVOKE_ADMIN_ACCESS:
    case ADMIN_ACC_DISABLE:
      // state.admins.filter(admin => admin._id !== payload._id)
      return {
        ...state,
        // admins: [payload, ...state.admins],
        adminLoading: false,
        errors: null,
      }
    case REVOKE_ADMIN_ACCESS_FAILED:
    case ADMIN_ACC_DISABLE_FAILED:
      return {
        ...state,
        errors: payload,
        adminLoading: false,
      }
    default:
      return state
  }
}
