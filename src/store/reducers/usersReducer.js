import {
  CHANGE_USER_SUB,
  CHANGE_USER_SUB_FAILED,
  DELETE_USER_ACC,
  DELETE_USER_ACC_FAILED,
  GET_ALL_USER_ACC,
  GET_ALL_USER_ACC_FAILED,
} from '../actions/adminActions'

const initialState = {
  usersLoading: true,
  users: null,
  errors: null,
}

export default (state = initialState, action) => {
  const { type, payload } = action
  switch (type) {
    case GET_ALL_USER_ACC:
      return {
        ...state,
        users: payload,
        errors: null,
        usersLoading: false,
      }
    case GET_ALL_USER_ACC_FAILED:
      return {
        ...state,
        errors: payload,
        users: null,
        usersLoading: false,
      }
    case CHANGE_USER_SUB:
      // state.users.filter(user => user._id !== payload._id)
      return {
        ...state,
        // users: [payload, ...state.users],
        usersLoading: false,
        errors: null,
      }
    case CHANGE_USER_SUB_FAILED:
      return {
        ...state,
        errors: payload,
        usersLoading: false,
      }
    case DELETE_USER_ACC:
      return {
        ...state,
        users: state.users.filter(user => user._id !== payload.id),
        usersLoading: false,
        errors: null,
      }
    case DELETE_USER_ACC_FAILED:
      return {
        ...state,
        errors: payload,
        usersLoading: false,
      }
    default:
      return state
  }
}
