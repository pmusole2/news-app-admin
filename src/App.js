import React, { Fragment, useEffect } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import { store } from './store/store'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'
import Navigation from './components/Navbar'
import Routes from './components/Routes'
import { setAuthToken } from './store/helpers'
import { getCurrentAdmin, LOGOUT } from './store/actions/authActions'

function App() {
  useEffect(() => {
    // Check for token
    if (localStorage.token) {
      setAuthToken(localStorage.token)
    }
    store.dispatch(getCurrentAdmin())

    // Log user out of all tabs
    window.addEventListener('storage', () => {
      if (!localStorage.token) store.dispatch({ type: LOGOUT })
    })
  }, [])
  return (
    <Provider store={store}>
      <Router>
        <Fragment>
          <Navigation />
          {/* <Alert /> */}
          <Switch>
            <Route component={Routes} />
          </Switch>
        </Fragment>
      </Router>
    </Provider>
  )
}

export default App
